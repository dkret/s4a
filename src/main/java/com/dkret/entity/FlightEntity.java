package com.dkret.entity;

import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "Flight")
public class FlightEntity {

    @Id
    private Integer flightId;
    @Min(1000)
    @Max(9999)
    private Integer flightNumber;
    private IataAirportCode departureAirportIATACode;
    private IataAirportCode arrivalAirportIATACode;
    private ZonedDateTime departureDate;
}
