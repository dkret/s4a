package com.dkret.repository;

import com.dkret.entity.FlightEntity;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightRepository extends JpaRepository<FlightEntity, Integer> {

    default Set<FlightEntity> findByDate(LocalDate date) {
        ZonedDateTime departureDate = date.atStartOfDay(ZoneId.systemDefault());
        return findByDepartureDateBetween(departureDate, departureDate.plusDays(1));
    }

    Set<FlightEntity> findByDepartureDateBetween(ZonedDateTime from, ZonedDateTime to);

    default Optional<FlightEntity> findByFlightIdAndDepartureDate(Integer flightId, LocalDate date) {
        ZonedDateTime departureDate = date.atStartOfDay(ZoneId.systemDefault());
        return findByFlightIdAndDepartureDateBetween(flightId, departureDate, departureDate.plusDays(1));
    }

    Optional<FlightEntity> findByFlightIdAndDepartureDateBetween(Integer flightId, ZonedDateTime from,
            ZonedDateTime to);
}
