package com.dkret.controller;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.dkret.controller.response.CargoInformationResponse;
import com.dkret.service.CargoService;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class CargoController {

    private final CargoService service;

    @GetMapping(path = "/cargo-information/{flightNumber}/{date}", produces = APPLICATION_JSON_VALUE)
    ResponseEntity<CargoInformationResponse> getCargoInformation(
            @PathVariable("flightNumber") Integer flightNumber,
            @PathVariable("date") String date) {
        return new ResponseEntity<>(service.getCargoInformation(flightNumber, LocalDate.parse(date)), OK);
    }
}
