package com.dkret.service

import com.dkret.entity.CargoEntity
import com.dkret.entity.FlightEntity
import com.dkret.entity.LoadingEntity
import com.dkret.repository.CargoRepository
import com.dkret.repository.FlightRepository
import spock.lang.Specification

import java.time.LocalDate

import static com.dkret.entity.IataAirportCode.ANC
import static com.dkret.entity.IataAirportCode.GDN
import static com.dkret.entity.WeightUnit.kg
import static com.dkret.entity.WeightUnit.lb

class CargoServiceSpec extends Specification {

    CargoRepository cargoRepository = Mock()
    FlightRepository flightRepository = Mock()

    CargoService service = new CargoService(cargoRepository, flightRepository)

    def 'should return cargo information'() {
        given:
        def flightId = 1;
        def date = LocalDate.of(2021, 1, 1)

        flightRepository.findByFlightIdAndDepartureDate(flightId, date) >> Optional.of(
                new FlightEntity(flightId: flightId, departureAirportIATACode: ANC, arrivalAirportIATACode: GDN))

        def baggage1 = new LoadingEntity(1, 10, kg, 4)
        def baggage2 = new LoadingEntity(2, 20, lb, 2)
        def cargo1 = new LoadingEntity(3, 20, kg, 5)
        def cargo2 = new LoadingEntity(4, 100, lb, 3)
        cargoRepository.findById(flightId) >> Optional.of(
                new CargoEntity(flightId: flightId, baggage: [baggage1, baggage2], cargo: [cargo1, cargo2]))

        when:
        def result = service.getCargoInformation(flightId, date)

        then:
        def expectedBaggageWeight = baggage1.getTotalWeightInKg() + baggage2.getTotalWeightInKg()
        def expectedCargoWeight = cargo1.getTotalWeightInKg() + cargo2.getTotalWeightInKg()

        result.baggageWeight == expectedBaggageWeight
        result.cargoWeight == expectedCargoWeight
        result.totalWeight == expectedBaggageWeight + expectedCargoWeight
    }
}
