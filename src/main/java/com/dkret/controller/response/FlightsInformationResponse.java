package com.dkret.controller.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FlightsInformationResponse {

    private Integer nrOfFlightsDepartingFromAirport;
    private Integer nrOfFlightsArrivingFromAirport;
    private Integer nrOfBaggagePiecesDepartingFromAirport;
    private Integer nrOfBaggagePiecesArrivingToAirport;

    public FlightsInformationResponse add(FlightsInformationResponse other) {
        this.nrOfFlightsDepartingFromAirport += other.nrOfFlightsDepartingFromAirport;
        this.nrOfFlightsArrivingFromAirport += other.nrOfFlightsArrivingFromAirport;
        this.nrOfBaggagePiecesDepartingFromAirport += other.nrOfBaggagePiecesDepartingFromAirport;
        this.nrOfBaggagePiecesArrivingToAirport += other.nrOfBaggagePiecesArrivingToAirport;
        return this;
    }
}
