package com.dkret.service;

import com.dkret.controller.response.FlightsInformationResponse;
import com.dkret.entity.CargoEntity;
import com.dkret.entity.FlightEntity;
import com.dkret.entity.IataAirportCode;
import com.dkret.entity.LoadingEntity;
import com.dkret.repository.CargoRepository;
import com.dkret.repository.FlightRepository;
import java.time.LocalDate;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FlightService {

    private final FlightRepository flightRepository;
    private final CargoRepository cargoRepository;

    public FlightsInformationResponse getFlightInformation(IataAirportCode iataAirportCode, LocalDate date)
            throws NotFoundException {
        return flightRepository.findByDate(date).stream()
                .filter(flight -> hasGivenArrivalOrDepartureIataCode(iataAirportCode, flight))
                .map(flight -> toFlightsInformationResponse(iataAirportCode, flight))
                .reduce(FlightsInformationResponse::add)
                .orElseThrow(NotFoundException::new);
    }

    private boolean hasGivenArrivalOrDepartureIataCode(IataAirportCode iataAirportCode, FlightEntity flight) {
        return flight.getArrivalAirportIATACode() == iataAirportCode
                || flight.getDepartureAirportIATACode() == iataAirportCode;
    }

    private FlightsInformationResponse toFlightsInformationResponse(IataAirportCode iataAirportCode,
            FlightEntity flight) {
        Optional<CargoEntity> cargo = cargoRepository.findById(flight.getFlightNumber());
        return flight.getArrivalAirportIATACode() == iataAirportCode
                ? new FlightsInformationResponse(0, 1, 0, getSumOfBaggagePieces(cargo))
                : new FlightsInformationResponse(1, 0, getSumOfBaggagePieces(cargo), 0);
    }

    private int getSumOfBaggagePieces(Optional<CargoEntity> cargo) {
        return cargo
                .map(cargoEntity -> cargoEntity.getBaggage().stream().mapToInt(LoadingEntity::getPieces).sum())
                .orElse(0);
    }
}
