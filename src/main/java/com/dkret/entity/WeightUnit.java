package com.dkret.entity;

public enum WeightUnit {
    kg,
    lb
}
