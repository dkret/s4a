package com.dkret.entity;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.LAZY;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Table(name = "Cargo")
public class CargoEntity {

    @Id
    private Integer flightId;
    @OneToMany(fetch = LAZY, cascade = ALL)
    private Set<LoadingEntity> baggage;
    @OneToMany(fetch = LAZY, cascade = ALL)
    private Set<LoadingEntity> cargo;
}
