package com.dkret.controller.response;

import com.dkret.entity.WeightUnit;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CargoInformationResponse {

    private Integer cargoWeight;
    private Integer baggageWeight;
    private Integer totalWeight;
    private WeightUnit weightUnit;
}
