package com.dkret.entity

import spock.lang.Specification
import spock.lang.Unroll

import static com.dkret.entity.WeightUnit.kg
import static com.dkret.entity.WeightUnit.lb

class LoadingEntityTest extends Specification {

    @Unroll
    def 'Should calculate total weight in kg'() {
        given:
        def weight = 10
        def pieces = 100
        def loadingEntity = new LoadingEntity(0, weight, weightUnit, pieces)

        when:
        def result = loadingEntity.totalWeightInKg

        then:
        result == expectedResult

        where:
        weightUnit || expectedResult
        kg         || 1000
        lb         || 453
    }
}
