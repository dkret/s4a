package com.dkret;

import com.dkret.entity.CargoEntity;
import com.dkret.entity.FlightEntity;
import com.dkret.repository.CargoRepository;
import com.dkret.repository.FlightRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

@Service
public class UglyDataPopulator {

    private final ObjectMapper mapper;
    private final FlightRepository flightRepository;
    private final CargoRepository cargoRepository;

    public UglyDataPopulator(FlightRepository flightRepository, CargoRepository cargoRepository) {
        this.mapper = new ObjectMapper();
        this.mapper.registerModule(new JavaTimeModule());
        this.mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX"));

        this.cargoRepository = cargoRepository;
        this.flightRepository = flightRepository;

        populateFlightTable();
        populateCargoTable();
    }

    private void populateFlightTable() {
        ClassPathResource resource = new ClassPathResource("Flight.json");
        try {
            String jsonString = new String(resource.getInputStream().readAllBytes());
            List<FlightEntity> flightEntities = mapper.readValue(jsonString, new TypeReference<>() {
            });
            flightRepository.saveAll(flightEntities);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void populateCargoTable() {
        ClassPathResource resource = new ClassPathResource("Cargo.json");
        try {
            String jsonString = new String(resource.getInputStream().readAllBytes());
            List<CargoEntity> cargoEntityList = mapper.readValue(jsonString, new TypeReference<>() {
            });
            cargoRepository.saveAll(cargoEntityList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
