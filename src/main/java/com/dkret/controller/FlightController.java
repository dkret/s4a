package com.dkret.controller;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.dkret.controller.response.FlightsInformationResponse;
import com.dkret.entity.IataAirportCode;
import com.dkret.service.FlightService;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class FlightController {

    private final FlightService service;

    @GetMapping(path = "/flight-information/{iataAirportCode}/{date}", produces = APPLICATION_JSON_VALUE)
    ResponseEntity<FlightsInformationResponse> getCargoInformation(
            @PathVariable("iataAirportCode") IataAirportCode iataAirportCode,
            @PathVariable("date") String date) throws NotFoundException {
        return new ResponseEntity<>(service.getFlightInformation(iataAirportCode, LocalDate.parse(date)), OK);
    }
}
