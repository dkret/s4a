package com.dkret.service;

import static com.dkret.entity.WeightUnit.kg;

import com.dkret.controller.response.CargoInformationResponse;
import com.dkret.entity.CargoEntity;
import com.dkret.entity.LoadingEntity;
import com.dkret.repository.CargoRepository;
import com.dkret.repository.FlightRepository;
import java.time.LocalDate;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CargoService {

    private final CargoRepository cargoRepository;
    private final FlightRepository flightRepository;

    public CargoInformationResponse getCargoInformation(Integer flightId, LocalDate date) {
        return flightRepository.findByFlightIdAndDepartureDate(flightId, date)
                .flatMap(flight -> cargoRepository.findById(flight.getFlightId()))
                .map(this::toCargoInformationResponse)
                .orElseThrow();
    }

    private CargoInformationResponse toCargoInformationResponse(CargoEntity cargo) {
        int cargoSum = cargo.getCargo().stream().mapToInt(LoadingEntity::getTotalWeightInKg).sum();
        int baggageSum = cargo.getBaggage().stream().mapToInt(LoadingEntity::getTotalWeightInKg).sum();
        int totalSum = cargoSum + baggageSum;
        return new CargoInformationResponse(cargoSum, baggageSum, totalSum, kg);
    }
}
