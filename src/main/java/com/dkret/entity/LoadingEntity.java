package com.dkret.entity;

import static com.dkret.entity.WeightUnit.kg;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Table(name = "Loading")
public class LoadingEntity {

    public static final double CONVERSION_FACTOR = 2.2046;
    @Id
    private Long id;
    @Min(1)
    @Max(999)
    private Integer weight;
    private WeightUnit weightUnit;
    @Min(1)
    @Max(999)
    private Integer pieces;

    public Integer getTotalWeightInKg() {
        if (weightUnit == kg) {
            return weight * pieces;
        }
        Double weight = convertWeightToKg() * pieces;
        return weight.intValue();
    }

    private Double convertWeightToKg() {
        return weight / CONVERSION_FACTOR;
    }
}
