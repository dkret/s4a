package com.dkret.entity;

public enum IataAirportCode {
    SEA,
    YYZ,
    YYT,
    ANC,
    LAX,
    MIT,
    LEW,
    GDN,
    KRK,
    PPX
}
