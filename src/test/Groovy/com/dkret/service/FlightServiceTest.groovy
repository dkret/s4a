package com.dkret.service

import com.dkret.entity.CargoEntity
import com.dkret.entity.FlightEntity
import com.dkret.entity.LoadingEntity
import com.dkret.repository.CargoRepository
import com.dkret.repository.FlightRepository
import spock.lang.Specification

import java.time.LocalDate
import java.time.ZoneId

import static com.dkret.entity.IataAirportCode.*
import static com.dkret.entity.WeightUnit.kg
import static com.dkret.entity.WeightUnit.lb

class FlightServiceTest extends Specification {

    CargoRepository cargoRepository = Mock()
    FlightRepository flightRepository = Mock()

    FlightService service = new FlightService(flightRepository, cargoRepository)

    def 'should return flight informations'() {
        given:
        def flightId1 = 10
        def flightId2 = 20
        def airportCode = GDN
        def date = LocalDate.of(2021, 1, 1)
        def zonedDate = date.atStartOfDay(ZoneId.systemDefault())

        flightRepository.findByDate(date) >> [new FlightEntity(0, flightId1, airportCode, KRK, zonedDate),
                                              new FlightEntity(1, flightId2, LAX, airportCode, zonedDate)]

        def baggage1 = new LoadingEntity(1, 10, kg, 4)
        def baggage2 = new LoadingEntity(2, 20, lb, 2)
        def baggage3 = new LoadingEntity(3, 5, lb, 3)
        def cargo1 = new LoadingEntity(3, 20, kg, 5)
        def cargo2 = new LoadingEntity(4, 100, lb, 3)

        cargoRepository.findById(flightId1) >> Optional.of(new CargoEntity(flightId1, Set.of(baggage1), Set.of(cargo1)))
        cargoRepository.findById(flightId2) >> Optional.of(new CargoEntity(flightId2, Set.of(baggage2, baggage3), Set.of(cargo2)))

        when:
        def result = service.getFlightInformation(airportCode, date)

        then:
        result.nrOfFlightsDepartingFromAirport == 1
        result.nrOfFlightsArrivingFromAirport == 1
        result.nrOfBaggagePiecesDepartingFromAirport == 4
        result.nrOfBaggagePiecesArrivingToAirport == 5
    }
}
